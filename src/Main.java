public class Main {

    private static final char BLACK_LETTER = 'ч';
    private static final char WHITE_LETTER = 'б';
    private static final String WHITE_SPACE = " ";
    private static final int ARRAY_SIZE = 10;

    private static char fill(int row, int column) {
        char ch;
        if ((row + column) % 2 == 0) {
            ch = BLACK_LETTER;
        } else {
            ch = WHITE_LETTER;
        }
        return ch;
    }

    private static char[][] gen() {
        char[][] arr = new char[ARRAY_SIZE][ARRAY_SIZE];
        for (int i = 0; i < ARRAY_SIZE; i++) {
            for (int j = 0; j < ARRAY_SIZE; j++) {
                arr[i][j] = fill(i, j);
            }
        }
        return arr;
    }

    private static void print(char[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + WHITE_SPACE);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

        char[][] arr = gen();

        print(arr);

    }
}